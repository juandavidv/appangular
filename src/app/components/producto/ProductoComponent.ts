import { Component, OnInit } from '@angular/core';
import { PaisService } from '../../services/pais.service';
import { PaisModel } from '../../models/pais.model';
@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {
  paises: PaisModel[];
  pais: PaisModel;
  constructor(private paisService: PaisService) {
    this.pais = new PaisModel();
    this.paisService.getCountries().subscribe(result => {
      this.paises = result as PaisModel[];
      console.log(this.paises);
    });
  }
  ngOnInit() {
  }
  changePais(e) {
    this.pais.alpha3Code = e.target.value;
    console.log(this.pais.alpha3Code);
  }
}
