import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardsComponent } from './components/dashboards/dashboards.component';
import { ProductosComponent } from './components/productos/productos.component';
import { ProductoComponent } from './components/producto/ProductoComponent';
import { productoModel } from './models/producto.model';


const routes: Routes = [

{ path: '' , component: LoginComponent},
{ path: 'login' , component: LoginComponent},
{ path: 'dashboard' , component: DashboardsComponent},
{ path: 'productos' , component: ProductosComponent},
{ path: 'producto' , component: ProductoComponent},
{ path: 'producto' , Model: productoModel},
{ path: '**' , component: LoginComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }








